package com.model;
import java.security.NoSuchAlgorithmException;  
import java.security.MessageDigest;  

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Employee {

	@Id@GeneratedValue
	private int empId;
	private String empName;
	private double salary;
	private String gender;
	private String country;
	private Date doj;
	@Column(unique = true)
	private String emailId;
	private String password;
	
	@ManyToOne
	@JoinColumn(name="deptId")
	Department department;
	
	public Employee() {
	}

	public Employee(int empId, String empName, double salary, String gender, String country, Date doj, String emailId,
			String password) {
		this.empId = empId;
		this.empName = empName;
		this.salary = salary;
		this.gender = gender;
		this.country = country;
		this.doj = doj;
		this.emailId = emailId;
		this.password = password;
	}
	
	

	public Department getDepartment() {
		return department;
	}
	public void setDepartment(Department department) {
		this.department = department;
	}

	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}

	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}

	public Date getDoj() {
		return doj;
	}
	public void setDoj(Date doj) {
		this.doj = doj;
	}

	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
//		String encrption="";
//		for(int i=0;i<password.length();i++){
//		    int n=password.charAt(i);
//		    n+=3;
//		    char a=(char)(n);
//
//			encrption+=a;
//		}
//		System.out.println(encrption);
		 String encryptedpassword = null; 
		  try   
	        {  
	            /* MessageDigest instance for MD5. */  
	            MessageDigest m = MessageDigest.getInstance("MD5");  
	              
	            /* Add plain-text password bytes to digest using MD5 update(
	             * ) method. */  
	            m.update(password.getBytes());  
	              
	            /* Convert the hash value into bytes */   
	            byte[] bytes = m.digest();  
	              
	            /* The bytes array has bytes in decimal form. Converting it into hexadecimal format. */  
	            StringBuilder s = new StringBuilder();  
	            for(int i=0; i< bytes.length ;i++)  
	            {  
	                s.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));  
	            }  
	              
	            /* Complete hashed password in hexadecimal format */  
	            encryptedpassword = s.toString();  
	        }   
	        catch (NoSuchAlgorithmException e)   
	        {  
	            e.printStackTrace();  
	        }  
		this.password =encryptedpassword;
	}

	@Override
	public String toString() {
		return "Employee [empId=" + empId + ", empName=" + empName + ", salary=" + salary + ", gender=" + gender
				+ ", country=" + country + ", doj=" + doj + ", emailId=" + emailId + ", password=" + password
				+ ", department=" + department + "]";
	}

	
}