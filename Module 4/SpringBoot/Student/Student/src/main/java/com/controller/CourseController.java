package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.CourseDao;
import com.model.Course;

@RestController
public class CourseController {
	
	@Autowired
	CourseDao deptDao;
	
	@GetMapping("getAllCourses")
	public List<Course> getAllCourses() {
		return deptDao.getAllCourses();
	}
	
	@GetMapping("getCourseById/{id}")
	public Course getCourseById(@PathVariable("id") int deptId) {
		return deptDao.getCourseById(deptId);
	}
		
	@PostMapping("addCourse")
	public Course addCourse(@RequestBody Course dept) {
		return deptDao.addCourse(dept);
	}
	
	@PutMapping("updateCourse")
	public Course updateCourse(@RequestBody Course dept) {
		return deptDao.updateCourse(dept);
	}
	
	@DeleteMapping("deleteCourseById/{id}")
	public String deleteCourseById(@PathVariable("id") int deptId) {
		deptDao.deleteCourseById(deptId);
		return "Course Deleted Successfully!!";
	}
}