import { Component } from '@angular/core';

@Component({
  selector: 'app-test2',
  templateUrl: './test2.component.html',
  styleUrl: './test2.component.css'
})
export class Test2Component {

Id: number;
Name: string;
age: number;
hobbies: any;
address: any;
streetNo: number;
city: any;
state :any;


constructor() {
  // alert('Constructor Invoked...');

  this.Id = 101;
  this.Name = 'Pallavi';
  this.age = 25;
  this.hobbies = ['Running', 'Swimming', 'Reading', 'Sleeping']
  this.address = {this:[this.streetNo = 15,this.city = ['Hyderabad'],this.state = 'Telangana']};
}

ngOnInit() {
  //alert('ngOnInit Invoked...');
}

submit() {
  alert("Id:" + this.Id + "\n" + "Name: " + this.Name);
 
}

}
