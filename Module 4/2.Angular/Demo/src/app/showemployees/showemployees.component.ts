import { Component } from '@angular/core';

@Component({
  selector: 'app-showemployees',
  templateUrl: './showemployees.component.html',
  styleUrl: './showemployees.component.css'
})
export class ShowemployeesComponent {

  employees: any; 

  //Date Format: mm-DD-YYYY
  constructor() {
    this.employees = [
      {empId:101, empName:'Pallavi', salary:1212.12, gender:'Female',   country:'IND', doj:'06-13-2018'},
      {empId:102, empName:'Sai',  salary:2323.23, gender:'Male',   country:'CHI', doj:'07-14-2017'},
      {empId:103, empName:'Vyshu', salary:3434.34, gender:'Female', country:'USA', doj:'08-15-2016'},
      {empId:104, empName:'Navya',  salary:4545.45, gender:'Female', country:'FRA', doj:'09-16-2015'},
      {empId:105, empName:'Charan',  salary:5656.56, gender:'Male',   country:'NEP', doj:'10-17-2014'}
    ];
  }

}
